extern crate termion;

use std::io::Write;

pub trait Contains {
    fn contains(&self, n: u8) -> bool;
}

#[derive(Clone, Copy, Debug)]
pub struct PossibleValues {
    value_bit_set: u16,
}

impl Contains for PossibleValues {
    fn contains(&self, n: u8) -> bool {
        let m = 1 << (n - 1);
        m & self.value_bit_set == m
    }
}

impl Default for PossibleValues {
    fn default() -> PossibleValues {
        PossibleValues { value_bit_set: 511 }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Value {
    PresetValue(u8),
    KnownValue(u8),
    UnknownValue(PossibleValues),
}

impl Default for Value {
    fn default() -> Value {
        Value::UnknownValue(PossibleValues::default())
    }
}

impl Contains for Value {
    fn contains(&self, n: u8) -> bool {
        match self {
            Value::PresetValue(x) => *x == n,
            Value::KnownValue(x) => *x == n,
            Value::UnknownValue(pv) => pv.contains(n),
        }
    }
}

pub struct Puzzle {
    pub values: [[Value; 9]; 9],
}

pub struct PuzzleWriter<'a, W: Write> {
    pub puzzle: &'a mut Puzzle,
    screen: &'a mut W,
    x: u16,
    y: u16,
}

fn default_values() -> [Value; 9] {
    [Value::default(); 9]
}

fn all_default_values() -> [[Value; 9]; 9] {
    [default_values(); 9]
}

impl Puzzle {
    pub fn new() -> Puzzle {
        let vs = all_default_values();
        Puzzle { values: vs }
    }
}

const TOP    :&str = "┏━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━┳━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━┳━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━┓";
const REG    :&str = "┃         │         │         ┃         │         │         ┃         │         │         ┃";
const SEP    :&str = "┠─────────┼─────────┼─────────╂─────────┼─────────┼─────────╂─────────┼─────────┼─────────┨";
const SEP2   :&str = "┣━━━━━━━━━┿━━━━━━━━━┿━━━━━━━━━╋━━━━━━━━━┿━━━━━━━━━┿━━━━━━━━━╋━━━━━━━━━┿━━━━━━━━━┿━━━━━━━━━┫";
const BOTTOM :&str = "┗━━━━━━━━━┷━━━━━━━━━┷━━━━━━━━━┻━━━━━━━━━┷━━━━━━━━━┷━━━━━━━━━┻━━━━━━━━━┷━━━━━━━━━┷━━━━━━━━━┛";

impl<'a, W: Write> PuzzleWriter<'a, W> {
    pub fn puzzle_width() -> u16 {
        TOP.chars().count() as u16
    }

    pub fn puzzle_height() -> u16 {
        37
    }

    pub fn new(
        puzzle: &'a mut Puzzle,
        screen: &'a mut W,
        start_point: Option<(u16, u16)>,
    ) -> PuzzleWriter<'a, W> {
        let (x, y) = start_point.unwrap_or((1, 1));
        PuzzleWriter {
            puzzle,
            screen,
            x,
            y,
        }
    }

    pub fn init(&mut self) {
        let x = self.x;
        let mut y = self.y;
        write!(self.screen, "{}{}", termion::cursor::Goto(x, y), TOP).unwrap();
        y += 1;
        for i in 0..3 {
            for j in 0..3 {
                for _ in 0..3 {
                    write!(self.screen, "{}{}", termion::cursor::Goto(x, y), REG).unwrap();
                    y += 1;
                }
                if j != 2 {
                    write!(self.screen, "{}{}", termion::cursor::Goto(x, y), SEP).unwrap();
                    y += 1;
                }
            }
            if i != 2 {
                write!(self.screen, "{}{}", termion::cursor::Goto(x, y), SEP2).unwrap();
                y += 1;
            }
        }
        write!(self.screen, "{}{}", termion::cursor::Goto(x, y), BOTTOM).unwrap();
        self.draw_all_cells();
        self.screen.flush().unwrap();
    }

    pub fn draw_all_cells(&mut self) {
        const XS: [usize; 9] = [0, 1, 2, 3, 4, 5, 6, 7, 8];
        for x in XS.iter() {
            for y in XS.iter() {
                self.draw_cell(*x, *y);
            }
        }
    }

    pub fn draw_cell(&mut self, x: usize, y: usize) {
        let center_x = self.x + 5 + (10 * x) as u16;
        let center_y = self.y + 2 + (4 * y) as u16;
        match self.puzzle.values[x][y] {
            Value::PresetValue(n) => {
                write!(
                    self.screen,
                    "{}{}{}{}",
                    termion::style::Bold,
                    termion::cursor::Goto(center_x, center_y),
                    n,
                    termion::style::Reset
                )
                .unwrap();
            }
            Value::KnownValue(n) => {
                write!(
                    self.screen,
                    "{}{}",
                    termion::cursor::Goto(center_x, center_y),
                    n
                )
                .unwrap();
            }
            Value::UnknownValue(possible_values) => {}
        }
    }
}
