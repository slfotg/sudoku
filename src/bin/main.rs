extern crate sudoku;
extern crate termion;

use std::io::stdout;
use std::io::Write;
use std::{thread, time};
use termion::raw::IntoRawMode;
use termion::screen::*;
use termion::terminal_size;

use sudoku::Puzzle;
use sudoku::PuzzleWriter;
use sudoku::Value;

fn main() {
    let mut screen = AlternateScreen::from(stdout().into_raw_mode().unwrap());
    write!(screen, "{}", termion::cursor::Hide).unwrap();
    write!(screen, "{}", termion::clear::All).unwrap();
    let mut puzzle = Puzzle::new();
    puzzle.values[0][0] = Value::PresetValue(3);

    let start_point = compute_start_point(&mut screen);
    let mut puzzle_writer = PuzzleWriter::new(&mut puzzle, &mut screen, start_point);

    puzzle_writer.init();
    let ten_millis = time::Duration::from_secs(5);

    thread::sleep(ten_millis);
}

fn compute_start_point<W: Write>(_screen: &mut W) -> Option<(u16, u16)> {
    let (x, y) = terminal_size().unwrap();
    let puzzle_width = PuzzleWriter::<W>::puzzle_width();
    let puzzle_height = PuzzleWriter::<W>::puzzle_height();
    if x <= puzzle_width || y <= puzzle_height {
        Option::None
    } else {
        Option::Some(((x - puzzle_width) / 2, (y - puzzle_height) / 2))
    }
}
